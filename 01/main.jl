using DelimitedFiles
using Printf

numbers = readdlm("./input.txt")

# First part
for i in numbers, j in numbers
    if i+j == 2020
        display(i*j)
    end
end


# Second part
for i in numbers, j in numbers, k in numbers
    if i+j+k == 2020
        println("")
        @printf("%d",i*j*k);
    end
end
