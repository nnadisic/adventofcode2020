

function puzzle1(datafilename)
    # Read data
    data = read(datafilename, String)
    groups = split(data, "\n\n")
    sumcounts = 0
    for g in groups
        curstr = replace(g, "\n"=>"")
        groupcount = length(union(split(curstr, "")))
        sumcounts += groupcount
    end
    return sumcounts
end


function puzzle2(datafilename)
    # Read data
    data = read(datafilename, String)
    groups = split(data, "\n\n")
    sumcounts = 0
    # Loop on groups
    for g in groups
        nbpersons = count("\n", g) + 1
        curstr = replace(g, "\n"=>"")
        # Build a dictionary to count the number of positive answer per question
        dict = Dict{Char,Int}()
        for c in curstr
            if get(dict, c, 0) == 0
                dict[c] = 1
            else
                dict[c] += 1
            end
        end
        display(nbpersons)
        # Find the questions that have 100% positive answers in the group
        groupcount = count(v == nbpersons for v in values(dict))
        display(groupcount)
        sumcounts += groupcount
    end
    return sumcounts
end


datafilename = "./input"
display(puzzle1(datafilename))
display(puzzle2(datafilename)) # quick and dirty: we add +1 "manually" before submission, to account for the last group, that has one "\n" too many
