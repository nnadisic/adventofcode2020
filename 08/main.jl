using DelimitedFiles

function puzzle1(datafilename)
    # Read data
    data = readdlm(datafilename)

    # Init variables
    accu = 0
    nextop = 1
    history = Set{Int}() # remember instructions we already ran
    # Loop on instructions
    while nextop != 0
        push!(history, nextop)
        op = data[nextop, 1]
        arg = data[nextop, 2]
        if op == "acc"
            accu += arg
            nextop += 1
        elseif op == "jmp"
            nextop += arg
        elseif op == "nop"
            nextop += 1
        end
        # Stop if begin second loop
        if nextop in history
            nextop = 0
        end
    end

    return accu
end


function puzzle2(datafilename)
    # Read data
    data = readdlm(datafilename)
    nbinstr = size(data, 1)

    # Create the list of all possible programs when changing one nop or jmp
    programs = []
    for i in 1:nbinstr
        op = data[i,1]
        if op == "acc" # do nothing
        else
            prog = copy(data[:,1])
            if op == "jmp"
                prog[i] = "nop"
            elseif op == "nop"
                prog[i] = "jmp"
            end
            push!(programs, prog)
        end
    end

    # Test all programs, keep the one with no infinite loop
    progfound = 0
    retaccu = 0
    for prog in programs
        # Init variables
        accu = 0
        nextop = 1
        history = Set{Int}() # remember instructions we already ran
        # Loop on instructions
        while nextop != 0
            push!(history, nextop)
            op = prog[nextop]
            arg = data[nextop, 2]
            if op == "acc"
                accu += arg
                nextop += 1
            elseif op == "jmp"
                nextop += arg
            elseif op == "nop"
                nextop += 1
            end
            # Stop if begin second loop
            if nextop in history
                nextop = 0
            end
            # Exit succesfully if try to access instruc below the last one
            if nextop > nbinstr
                nextop = 0
                progfound = 1
                retaccu = accu
            end
        end
        if progfound == 1
            break
        end
    end
    return retaccu
end

display(puzzle1("./toyexample"))

datafilename = "./input"
display(puzzle1(datafilename))

display(puzzle2(datafilename))
