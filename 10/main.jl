using DelimitedFiles

function puzzle1(datafilename)
    # Read data
    data = readdlm(datafilename, Int)[:,1] # readdlm generates a 2d array with one column, we only keep 1d

    # Sort vector and add 0 (initial plug) and entry = lastentry + 3 (final device)
    sort!(data)
    pushfirst!(data, 0)
    push!(data, data[end]+3)

    # Initialize counters
    counters = [0, 0, 0]
    # Count the differences
    for i in 2:length(data)
        counters[data[i]-data[i-1]] += 1
    end

    return counters
end


function puzzle2(datafilename)
    # Read data
    data = readdlm(datafilename, Int)[:,1] # readdlm generates a 2d array with one column, we only keep 1d

    # Sort vector and add 0 (initial plug) and entry = lastentry + 3 (final device)
    sort!(data)
    pushfirst!(data, 0)
    push!(data, data[end]+3)

    # Count all possibles path to an adapter from the last device
    nbcombiperadapter = Dict{Int, Int}()
    # Initialize with the path to last device (only one path possible)
    nbcombiperadapter[data[end]] = 1
    # Loop, from last device to initial plug
    for i in length(data)-1:-1:1
        # Sum the cumulated numbers of combis from all adapters connected to the current one
        curnbcombi = 0
        for j in 1:3
            curnbcombi += get(nbcombiperadapter, data[i]+j, 0)
        end
        nbcombiperadapter[data[i]] = curnbcombi
    end

    # Return number of path from last device (end) to initial plug (0)
    return nbcombiperadapter[0]
end


datafilename = "./input"
# display(puzzle1(datafilename))
display(puzzle2("./toyex")) # should give 19208
display(puzzle2(datafilename))


##############################################################################
# This solution works for the toy example but it is way too expensive for the actual
# input. I keep it here just for history.

# function puzzle2old(datafilename)
#     # Read data
#     data = readdlm(datafilename, Int)[:,1] # readdlm generates a 2d array with one column, we only keep 1d

#     # Sort vector and add 0 (initial plug) and entry = lastentry + 3 (final device)
#     sort!(data)
#     pushfirst!(data, 0)
#     push!(data, data[end]+3)

#     # Count all possibles working combinations
#     nbcombi = 0
#     # Store combinations to explore, begin with the root
#     combinations = [[data[1]]]
#     # Explore combinations in a tree-like spirit
#     while !isempty(combinations)
#         # Select a combination
#         curcombi = pop!(combinations)
#         # If complete, increment counter and pass; if not complete, keep exploring down
#         if curcombi[end] == data[end]
#             nbcombi += 1
#         else
#             for i in 1:3
#                 idxcurindata = findfirst(isequal(curcombi[end]), data)
#                 if idxcurindata+i <= length(data)
#                     nextentry = data[idxcurindata+i]
#                     if nextentry - data[idxcurindata] <= 3
#                         push!(combinations, [curcombi..., nextentry])
#                     end
#                 end
#             end
#         end
#     end

#     return nbcombi
# end
