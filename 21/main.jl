
function puzzle(filename; part=1)
    # Read data
    file = open(filename)
    lines = readlines(file)
    nblines = length(lines)

    # Store ingredients and allergens, a list of strings for every line
    listingr = []
    listaller = []

    # Store uniquely all ingredients
    allingredients = Dict{String, Int}()

    # Parse lines to extract ingredients and allergens
    for l in lines
        rawingr, rawaller = split(l, " (contains ")
        ingrs = split(rawingr)
        allers = split(filter(c -> !(c in [',', ')']), rawaller))
        push!(listingr, ingrs)
        push!(listaller, allers)
        for i in ingrs
            if !haskey(allingredients, i)
                allingredients[i] = 0
            end
            allingredients[i] += 1
        end
    end

    # Store for each allergen a list of lists of possible corresponding ingredients
    possiblematch = Dict{String, Vector{Vector{String}}}()
    for i in 1:nblines
        for aller in listaller[i]
            if !haskey(possiblematch, aller)
                possiblematch[aller] = []
            end
            push!(possiblematch[aller], listingr[i])
        end
    end

    # Remember ingredients that can potentially contain allergen
    unsafeingrs = Set{String}()

    # Get the intersections of list of ingredients per allergen (narrow down potential ingr)
    intersectmatch = Dict{String, Vector{String}}()
    for (aller, ingrs) in possiblematch
        interingr = intersect(ingrs...)
        intersectmatch[aller] = interingr
        for i in interingr
            push!(unsafeingrs, i)
        end
    end
    # display(intersectmatch)

    # Find ingredients that cannot possibly contain allergens
    safeingr = filter(i -> !(i in unsafeingrs), keys(allingredients))
    # display(safeingr)

    # PART 1
    if part == 1
        # Sum occurences of safe ingredients
        nbappearsafe = 0
        for i in safeingr
            nbappearsafe += allingredients[i]
        end
        return nbappearsafe
    end

    # BELOW IS PART 2

    # Sort allergens by length of list of ingredients
    sortedlist = sort(collect(intersectmatch), by=x->length(x[2]))
    # Find for each allergen a unique ingredient
    exactmatches = Dict{String,String}()
    while length(exactmatches) != length(sortedlist)
        for (aller, ingrs) in sortedlist
            if length(ingrs) == 1
                curingr = ingrs[1]
                exactmatches[aller] = curingr
                for (_, otheringr) in values(sortedlist)
                    filter!(e->e!=curingr, otheringr)
                end
            end
        end
    end
    # display(exactmatches)

    canonicallist = sort(collect(exactmatches), by=x->x[1])
    retstring = ""
    for (aller, ingr) in canonicallist
        retstring = retstring * ingr * ","
    end

    return strip(retstring, [','])
end


println("Puzzle 1")
display(puzzle("./toy")) # should give 5
display(puzzle("./input"))

println("\nPuzzle 2")
display(puzzle("./toy", part=2)) # should give mxmxvkd,sqjhc,fvjkl
display(puzzle("./input", part=2))
