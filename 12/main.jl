using DelimitedFiles

function puzzle1(filename)
    # Read data
    data = readdlm(filename)[:,1]

    # Init variables
    posx = 0 # north (positive) and south (negative)
    posy = 0 # east (positive) and west (negative)
    orientation = 0 # between 0 and 360, 0 means facing east, 90 means facing north

    # Loop on instructions
    for instr in data
        action = first(instr)
        val = parse(Int, instr[2:end])

        if action == 'F'
            modo = mod(orientation, 0:359)
            if modo == 0
                action = 'E'
            elseif modo == 90
                action = 'N'
            elseif modo == 180
                action = 'W'
            elseif modo == 270
                action = 'S'
            else
                error("Orientation not multiple of 90 :(")
            end
        end

        if action == 'N'
            posx += val
        elseif action == 'S'
            posx -= val
        elseif action == 'E'
            posy += val
        elseif action == 'W'
            posy -= val
        elseif action == 'L'
            orientation += val
        elseif action == 'R'
            orientation -= val
        end

    end

    return abs(posx) + abs(posy)
end


function puzzle2(filename)
    # Read data
    data = readdlm(filename)[:,1]

    # Init variables
    posx = 0 # north (positive) and south (negative)
    posy = 0 # east (positive) and west (negative)
    wpx = 1 # relative north-south position of waypoint
    wpy = 10 # relative east-west position of waypoint

    # Loop on instructions
    for instr in data
        action = first(instr)
        val = parse(Int, instr[2:end])

        if action == 'F'
            posx += val * wpx
            posy += val * wpy
        elseif action == 'N'
            wpx += val
        elseif action == 'S'
            wpx -= val
        elseif action == 'E'
            wpy += val
        elseif action == 'W'
            wpy -= val
        elseif action == 'L'
            for _ in 1:(val/90)
                oldwpx = wpx
                oldwpy = wpy
                wpx = oldwpy
                wpy = -oldwpx
            end
        elseif action == 'R'
            for _ in 1:(val/90)
                oldwpx = wpx
                oldwpy = wpy
                wpx = -oldwpy
                wpy = oldwpx
            end
        end

    end

    return abs(posx) + abs(posy)
end


display(puzzle1("./input"))
display(puzzle2("./input"))
