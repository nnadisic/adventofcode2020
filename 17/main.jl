using DelimitedFiles

function puzzle1(filename)
    # Read data
    data = readdlm(filename)

    # The grid state is stored as a set of tuples representing the coord of active cubes
    activecubes = Set{Tuple{Int,Int,Int}}()
    # Initial state
    for (x, row) in enumerate(data)
        for (y, val) in enumerate(row)
            if val == '#'
                push!(activecubes, (x,y,0))
            end
        end
    end

    # Update cycles
    for cycle in 1:6
        nextactivecubes = Set{Tuple{Int,Int,Int}}()
        # Find grid bounds
        minx, maxx = extrema(x->x[1], activecubes)
        miny, maxy = extrema(x->x[2], activecubes)
        minz, maxz = extrema(x->x[3], activecubes)
        # Loop on all cubes
        for x in minx-1:maxx+1, y in miny-1:maxy+1, z in minz-1:maxz+1
            # For current cube
            curcubeisactive = false
            nbneighbors = 0
            # If current is active, count -1 here because it will be counted in neighbors
            if (x,y,z) in activecubes
                curcubeisactive = true
                nbneighbors -= 1
            end
            # Loop on neighbors and count active ones
            for i in x-1:x+1, j in y-1:y+1, k in z-1:z+1
                if (i,j,k) in activecubes
                    nbneighbors += 1
                end
            end
            if nbneighbors == 3 || (curcubeisactive && nbneighbors == 2)
                push!(nextactivecubes, (x,y,z))
            end
        end

        # Update current grid
        activecubes = copy(nextactivecubes)
    end

    return length(activecubes)
end


function puzzle2(filename)
    # Read data
    data = readdlm(filename)

    # The grid state is stored as a set of tuples representing the coord of active cubes
    activecubes = Set{Tuple{Int,Int,Int,Int}}()
    # Initial state
    for (x, row) in enumerate(data)
        for (y, val) in enumerate(row)
            if val == '#'
                push!(activecubes, (x,y,0,0))
            end
        end
    end

    # Update cycles
    for cycle in 1:6
        nextactivecubes = Set{Tuple{Int,Int,Int,Int}}()
        # Find grid bounds
        minx, maxx = extrema(x->x[1], activecubes)
        miny, maxy = extrema(x->x[2], activecubes)
        minz, maxz = extrema(x->x[3], activecubes)
        mint, maxt = extrema(x->x[4], activecubes)
        # Loop on all cubes
        for x in minx-1:maxx+1, y in miny-1:maxy+1, z in minz-1:maxz+1, t in mint-1:maxt+1
            # For current cube
            curcubeisactive = false
            nbneighbors = 0
            # If current is active, count -1 here because it will be counted in neighbors
            if (x,y,z,t) in activecubes
                curcubeisactive = true
                nbneighbors -= 1
            end
            # Loop on neighbors and count active ones
            for i in x-1:x+1, j in y-1:y+1, k in z-1:z+1, l in t-1:t+1
                if (i,j,k,l) in activecubes
                    nbneighbors += 1
                end
            end
            if nbneighbors == 3 || (curcubeisactive && nbneighbors == 2)
                push!(nextactivecubes, (x,y,z,t))
            end
        end

        # Update current grid
        activecubes = copy(nextactivecubes)
    end

    return length(activecubes)
end

println("Puzzle 1")
display(puzzle1("./input"))

println("\nPuzzle 2")
display(puzzle2("./input"))
