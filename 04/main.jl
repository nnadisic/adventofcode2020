using DelimitedFiles

function puzzle1(datafilename)
    # Read data
    data = read(datafilename, String)
    documents = split(data, "\n\n")
    validcounter = 0
    for d in documents
        fields = [x[1:3] for x in split(d)]
        if ("ecl" in fields) && ("iyr" in fields) && ("hgt" in fields) && ("byr" in fields) && ("hcl" in fields) && ("eyr" in fields) && ("pid" in fields)
            validcounter += 1
        end
    end
    return validcounter
end

function puzzle2(datafilename)
    # Read data
    data = read(datafilename, String)
    documents = split(data, "\n\n")
    validcounter = 0
    # Loop on all documents
    for d in documents
        fields = split(d)
        # Build dictionary that stores the fields labels and values
        dict = Dict{String,String}()
        for f in fields
            label = f[1:3]
            value = f[5:end]
            dict[label] = value
        end

        isvalid = true

        if !(1920 <= parse(Int, get(dict, "byr", "0")) <= 2020)
            isvalid = false
        end

        if !(2010 <= parse(Int, get(dict, "iyr", "0")) <= 2020)
            isvalid = false
        end

        if !(2020 <= parse(Int, get(dict, "eyr", "0")) <= 2030)
            isvalid = false
        end

        height = get(dict, "hgt", "")
        if height == "" || length(height) < 4
            isvalid = false
        else
            value = parse(Int, height[1:end-2])
            unit = height[end-1:end]
            if !(unit == "cm" && (150 <= value <= 193)) && !(unit == "in" && (59 <= value <= 76))
                isvalid = false
            end
        end

        haircolor = get(dict, "hcl", "")
        if !(occursin(r"^#[0-9a-f]{1,6}$", haircolor))
            isvalid = false
        end

        eyecolor = get(dict, "ecl", "")
        if !(eyecolor in ["amb" "blu" "brn" "gry" "grn" "hzl" "oth"])
            isvalid = false
        end

        passportid = get(dict, "pid", "")
        if !(occursin(r"^[0-9]{9}$", passportid))
            isvalid = false
        end

        if isvalid
            validcounter += 1
        end
    end
    return validcounter
end


datafilename = "./input"
display(puzzle1(datafilename))
display(puzzle2(datafilename))
