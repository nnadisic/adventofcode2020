using DelimitedFiles

# ref for user-defined operator and their precedences:
# https://stackoverflow.com/questions/60321301/user-defined-infix-operator
# https://github.com/JuliaLang/julia/blob/master/src/julia-parser.scm
# \boxplus ⊞ has same precedence as +
# \boxtimes ⊠ has same precedence as *

# Def operator * with same precedence as +
⊞(a,b) = a * b

function puzzle1(filename)
    # Read data
    data = readdlm(filename, '\n', String)

    # Loop on expressions
    sumall = 0
    for l in data
        # Replace * with its equivalent of different precedence
        expr = replace(l, "*" => "⊞")
        # Evaluate expression and add to sum
        sumall += eval(Meta.parse(expr))
    end

    return sumall
end

# Def operator + with same precedence as *
⊠(a,b) = a + b

function puzzle2(filename)
    # Read data
    data = readdlm(filename, '\n', String)

    # Loop on expressions
    sumall = 0
    for l in data
        # Replace * with its equivalent of different precedence
        expr = replace(replace(l, "*" => "⊞"), "+" => "⊠")
        # Evaluate expression and add to sum
        sumall += eval(Meta.parse(expr))
    end

    return sumall
end


println("Puzzle 1")
display(puzzle1("./input"))

println("\nPuzzle 2")
display(puzzle2("./input"))
