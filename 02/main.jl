using DelimitedFiles
using Printf

function puzzle1(data)
    counter = 0
    for row in eachrow(data)
        lowerbound, upperbound = map(x->parse(Int,x),split(row[1],'-'))
        letter = string(row[2][1]) # the [1] is to keep the letter and discard the colon
        password = row[3]
        if lowerbound <= count(letter, password) <= upperbound
            counter += 1
        end
    end
    return counter
end

function puzzle2(data)
    counter = 0
    for row in eachrow(data)
        lowerbound, upperbound = map(x->parse(Int,x),split(row[1],'-'))
        letter = string(row[2][1]) # the [1] is to keep the letter and discard the colon
        password = row[3]
        if (string(password[lowerbound])==letter) + (string(password[upperbound])==letter) == 1
            counter += 1
        end
    end
    return counter
end

mydata = readdlm("./input")
display(puzzle1(mydata))
display(puzzle2(mydata))
