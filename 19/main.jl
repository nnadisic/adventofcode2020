
function puzzle1(filename)
    # Read data
    data = read(filename, String)
    rawrules, rawmsg = split(data, "\n\n")
    rules = split(rawrules, "\n")
    messages = split(rawmsg, "\n")

    # Process rules and store in dict
    dictrules = Dict{Int,String}()
    for r in rules
        number, content = split(r, ": ")
        dictrules[parse(Int, number)] = replace(content, "\"" => "")
    end
    # display(dictrules)

    # Generate a regex from the rules, recursively
    regexes = Dict{Int,String}()

    # Recursive function to create a regex from a rule
    function generateregex(nb)
        if haskey(regexes, nb)
            return regexes[nb]
        end
        rule = dictrules[nb]
        if !isnothing(match(r"\d+", rule))
            for m in eachmatch(r"\d+", rule)
                rule = replace(rule, m.match=>generateregex(parse(Int, m.match)); count=1)
            end
            rule = "(" * rule * ")"
        end
        regexes[nb] = rule
    end

    for r in keys(dictrules)
        generateregex(r)
    end

    # Remove space, add beginning- and end-of-line delimiters
    myregex = replace(regexes[0], " " => "")
    myregex = "^" * myregex * "\$"
    # println(myregex)

    regexobject = Regex(myregex)

    # Count messages that match the regex
    counter = 0
    for m in messages
        if occursin(regexobject, m)
            # display(m)
            counter += 1
        end
    end

    return counter
end


println("Puzzle 1")
display(puzzle1("./toy"))
display(puzzle1("./input"))
