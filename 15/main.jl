
function puzzle1(input, maxit)
    # Init array of numbers, beginning with the input (starting numbers)
    numbers = zeros(Int, maxit)
    numbers[1:length(input)] .= input
    # Dictionnary to store the spoken numbers
    spoken = Dict{Int, Int}()

    # Process starting numbers
    for i in 1:length(input)-1
        spoken[numbers[i]] = i
    end

    # Following numbers
    for i in length(input):maxit-1
        whenlastspoken = get(spoken, numbers[i], 0)
        spoken[numbers[i]] = i
        if whenlastspoken != 0
            numbers[i+1] = i - whenlastspoken
        end
    end

    return numbers[end]
end

# Input
toy = [3,1,2]
input = [11,0,1,10,5,19]

# Run
println("Puzzle 1")
display(puzzle1(toy, 2020)) # should give 1836
display(puzzle1(input, 2020))

println("\nPuzzle 2")
display(puzzle1(toy, 30000000)) # should give 362
display(puzzle1(input, 30000000))
