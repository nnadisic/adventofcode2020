
function puzzle1(filename)
    # Read data
    data = read(filename, String)
    rawtiles = split(data, "\n\n")

    # Store borders of tiles as a vector of 4 strings
    tiles = Dict{Int, Vector{String}}()
    # Parse input to build tiles
    for rt in rawtiles
        # Get tile ID
        id = parse(Int, match(r"\d+", rt).match)
        # Tile as a matrix of chars
        strtile = [split(line, "") for line in split(rt, "\n")[2:end]]
        tile = hcat(strtile...)
        # display(tile)
        tiles[id] = [join(tile[1,:]), join(tile[end,:]), join(tile[:,1]), join(tile[:,end])]
    end

    # Find corners, that is, tiles with 2 borders that do not match with other borders

    # Count for each tile the number of borders that match other borders
    bordermatchcounter = Dict{Int, Int}()
    # Init all counyters to 0
    for k in keys(tiles)
        bordermatchcounter[k] = 0
    end

    # Loop on tiles
    for (iter1, t1) in enumerate(tiles)
        for (iter2, t2) in enumerate(tiles)
            if iter1<iter2
                hasmatch = 0
                for b1 in t1[2]
                    for b2 in t2[2]
                        if (b1==b2) || (b1==reverse(b2))
                            hasmatch += 1
                            break
                        end
                    end
                end
                if hasmatch > 0
                    bordermatchcounter[t1[1]] += 1
                    bordermatchcounter[t2[1]] += 1
                end
            end
        end
    end

    corners = Int[]
    for c in bordermatchcounter
        if c[2] <= 2
            push!(corners, c[1])
        end
    end

    return prod(corners)
end


println("Puzzle 1")
display(puzzle1("./toy")) # should give 1951 * 3079 * 2971 * 1171 = 20899048083289
display(puzzle1("./input"))
