
function puzzle1(filename)
    # Read data
    file = open(filename)
    data = read(file, String)

    # Init decks
    rawdeck1, rawdeck2 = split(data, "\n\n")
    deck1 = [parse(Int, c) for c in split(rawdeck1, "\n")[2:end]]
    deck2 = [parse(Int, c) for c in split(rawdeck2, "\n")[2:end]]

    # Play !
    while !isempty(deck1) && !isempty(deck2)
        card1 = popfirst!(deck1)
        card2 = popfirst!(deck2)
        if card1 > card2
            push!(deck1, card1, card2)
        else
            push!(deck2, card2, card1)
        end
    end

    # Compute score
    score = 0
    winnerdeck = isempty(deck2) ? deck1 : deck2
    nbcards = length(winnerdeck)
    for (i, val) in enumerate(winnerdeck)
        score += (nbcards - i + 1) * val
    end

    return score
end

println("Puzzle 1")
display(puzzle1("./toy")) # should give 306
display(puzzle1("./input"))

######################################################################

function puzzle2(filename)
    # Read data
    file = open(filename)
    data = read(file, String)

    # Init decks
    rawdeck1, rawdeck2 = split(data, "\n\n")
    deck1 = [parse(Int, c) for c in split(rawdeck1, "\n")[2:end]]
    deck2 = [parse(Int, c) for c in split(rawdeck2, "\n")[2:end]]

    # Play !
    deck1, deck2, nbwinner = recursiveplay(deck1, deck2)

    # Compute score
    score = 0
    winnerdeck = nbwinner == 1 ? deck1 : deck2
    nbcards = length(winnerdeck)
    for (i, val) in enumerate(winnerdeck)
        score += (nbcards - i + 1) * val
    end

    return score
end

# Recursive play
function recursiveplay(d1, d2)
    # Copy input
    deck1 = copy(d1)
    deck2 = copy(d2)
    # Remember positions already played in this game
    playedpositions = Set{Vector{Int}}()
    while !isempty(deck1) && !isempty(deck2)
        # Test if position already played
        if vcat(deck1, -1, deck2) in playedpositions
            return deck1, deck2, 1
        end
        # Remember played position
        push!(playedpositions, vcat(deck1, -1, deck2))
        # Draw cards
        card1 = popfirst!(deck1)
        card2 = popfirst!(deck2)
        # Trigger recursive game if necessary, else compare cards
        if length(deck1) >= card1 && length(deck2) >= card2
            _, _, nbwinner = recursiveplay(deck1[1:card1], deck2[1:card2])
            if nbwinner == 1
                push!(deck1, card1, card2)
            else
                push!(deck2, card2, card1)
            end
        else
            if card1 > card2
                push!(deck1, card1, card2)
            else
                push!(deck2, card2, card1)
            end
        end
    end
    nbwinner = isempty(deck2) ? 1 : 2
    return deck1, deck2, nbwinner
end


println("Puzzle 2")
display(puzzle2("./toy")) # should give 291
display(puzzle2("./input"))
