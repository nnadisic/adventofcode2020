using DelimitedFiles

function puzzle1(datafilename)
    # Read data
    data = readdlm(datafilename)
    # Init value
    maxid = 0
    # Loop on all seats
    for seat in data
        rowstr = replace(replace(string(seat[1:7]), "F"=>"0"), "B"=>"1")
        rowint = parse(Int, rowstr, base=2)
        colstr = replace(replace(string(seat[8:10]), "L"=>"0"), "R"=>"1")
        colint = parse(Int, colstr, base=2)
        seatid = rowint * 8 + colint
        maxid = max(maxid, seatid)
    end
    return maxid
end

function puzzle2(datafilename)
    # Read data
    data = readdlm(datafilename)
    # Init list of seats, that we will keep sorted
    listofseats = Array{Integer}(undef, 0)
    # Loop to build all seats
    for seat in data
        rowstr = replace(replace(string(seat[1:7]), "F"=>"0"), "B"=>"1")
        rowint = parse(Int, rowstr, base=2)
        colstr = replace(replace(string(seat[8:10]), "L"=>"0"), "R"=>"1")
        colint = parse(Int, colstr, base=2)
        seatid = rowint * 8 + colint
        insert!(listofseats, searchsortedfirst(listofseats, seatid), seatid)
    end
    # Find the free seat
    freeseatid = 0
    for i in 2:length(listofseats)-1
        if !(listofseats[i]==listofseats[i-1]+1 && listofseats[i]==listofseats[i+1]-1)
            freeseatid = listofseats[i]-1
        end
    end
    return freeseatid
end


datafilename = "./input"
display(puzzle1(datafilename))
display(puzzle2(datafilename))
