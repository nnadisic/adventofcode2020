using DelimitedFiles

function puzzle1(filename)
    # Read data
    data = readdlm(filename)
    # display(data)

    # Init mask and memory
    mask = ""
    memory = Dict{Int, Int}()

    # Loop on lines
    for i in 1:size(data, 1)
        dest = data[i,1]
        val = data[i,3]
        if dest == "mask"
            mask = val
        else
            address = parse(Int, dest[5:end-1])
            memory[address] = applymask(val, mask)
        end
    end

    return sum(values(memory))
end

function applymask(value, maskstr)
    # Convert value and mask to arrays of strings
    valbin = split(bitstring(value)[end-35:end], "")
    mask = split(maskstr, "")
    # Apply mask
    for i in 1:36
        if mask[i] != "X"
            valbin[i] = mask[i]
        end
    end
    # Convert to int and return
    return parse(Int, string(valbin...), base=2)
end

######################################################################

function puzzle2(filename)
    # Read data
    data = readdlm(filename)
    # display(data)

    # Init mask and memory
    mask = ""
    memory = Dict{Int, Int}()

    # Loop on lines
    for i in 1:size(data, 1)
        dest = data[i,1]
        val = data[i,3]
        if dest == "mask"
            mask = val
        else
            address = parse(Int, dest[5:end-1])
            maskadresses!(address, mask, memory, val)
        end
    end

    return sum(values(memory))
end

function maskadresses!(addrstr, maskstr, memory, val)
    # Convert value and mask to arrays of strings
    addr = split(bitstring(addrstr)[end-35:end], "")
    mask = split(maskstr, "")
    # Apply the mask, overwrite address when entry is "1" or "X"
    for i in 1:36
        if mask[i] != "0"
            addr[i] = mask[i]
        end
    end
    # Generate all addresses by processing the "X"
    incompleteaddr = [addr]
    while !isempty(incompleteaddr)
        curaddr = pop!(incompleteaddr)
        iscomplete = true
        for i in 1:36
            if curaddr[i] == "X"
                iscomplete = false
                tmp0 = copy(curaddr)
                tmp1 = copy(curaddr)
                tmp0[i] = "0"
                tmp1[i] = "1"
                push!(incompleteaddr, tmp0, tmp1)
            end
        end
        if iscomplete
            memory[parse(Int, string(curaddr...), base=2)] = val
        end
    end
end


println("Puzzle 1")
display(puzzle1("./toy"))
display(puzzle1("./input"))

println("\nPuzzle 2")
display(puzzle2("./toy2"))
display(puzzle2("./input"))
