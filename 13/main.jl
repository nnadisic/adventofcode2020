using DelimitedFiles

function puzzle1(filename)
    # Read data
    data = readdlm(filename)
    earliesttimestamp = data[1,1]
    buses = Int[]
    for e in split(data[2,1], ',')
        if e != "x"
            push!(buses, parse(Int, e))
        end
    end
    # display(earliesttimestamp)
    # display(buses)

    # Find earliest bus
    bestbus = 0
    bestdelay = Inf
    for bus in buses
        for delay in 0:bus
            if isinteger((earliesttimestamp + delay) / bus)
                if delay < bestdelay
                    bestdelay = delay
                    bestbus = bus
                    break
                end
            end
        end
    end

    return bestbus * bestdelay
end


function puzzle2(filename)
    # Read data
    data = readdlm(filename)
    buses = Int[]
    busesdelays = Int[]
    for (idx, val) in enumerate(split(data[2,1], ','))
        if val != "x"
            push!(buses, parse(Int, val))
            push!(busesdelays, idx-1)
        end
    end
    # display(earliesttimestamp)
    # display(buses)
    # display(busesdelays)

    # Find max bus to use a stepsize and adapt delays relatively to it
    maxbus, maxbusid = findmax(buses)
    maxbusdelay = busesdelays[maxbusid]
    busesdelays = map(x -> x - maxbusdelay, busesdelays)

    # Dictionnay to follow buses taken into account
    curbuses = Dict{Int, Int}()
    for i in 1:length(buses)
        curbuses[buses[i]] = busesdelays[i]
    end
    # Remove maxbus (as we already begin with its value as a step size)
    delete!(curbuses, maxbus)

    t = BigInt(0)
    step = maxbus
    while !(isempty(curbuses))
        t += step
        for b in curbuses
            # b[1] is the key (bus id) and b[2] the value (corresp delay)
            if isinteger((t + b[2]) / b[1])
                step *= b[1]
                delete!(curbuses, b[1])
            end
        end
    end

    return t + busesdelays[1]
end

println("Puzzle 1")
display(puzzle1("./toy"))
display(puzzle1("./input"))

println("\nPuzzle 2")
display(puzzle2("./toy"))
display(puzzle2("./input"))
