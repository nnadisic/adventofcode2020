using DelimitedFiles
using Printf

function puzzle1(datafilename, sloperight, slopedown)
    # Read data
    data = readdlm(datafilename)
    nbrow = size(data,1)
    nbcol = length(data[1])
    # Build matrix of characters
    matchars = Matrix{Char}(undef, nbrow, nbcol)
    for i in 1:nbrow, j in 1:nbcol
        matchars[i,j] = data[i][j]
    end
    # display(matchars)
    # Init
    treecounter = 0
    curi = 1
    curj = 1
    # Main loop
    while curi <= nbrow
        # Test if a tree is here
        if matchars[curi,curj] == '#'
            treecounter += 1
        end
        # Go to the next step
        curi += slopedown
        curj += sloperight
        # Represent vertical pattern repetition
        if curj > nbcol
            curj -= nbcol
        end
    end
    return treecounter
end



datafilename = "./input"

# Puzzle 1
println("Puzzle 1")
display(puzzle1(datafilename, 3, 1))

# Puzzle 2
println("Puzzle 2")
display(puzzle1(datafilename, 1, 1))
display(puzzle1(datafilename, 3, 1))
display(puzzle1(datafilename, 5, 1))
display(puzzle1(datafilename, 7, 1))
display(puzzle1(datafilename, 1, 2))
