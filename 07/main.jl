

function puzzle1(datafilename)
    # Read data
    file = open(datafilename)
    lines = readlines(file)

    # Build a dictionary of colors
    dict = Dict{String,Int}()
    idx = 1
    for line in lines
        spl = split(line)
        color = spl[1]*spl[2]
        dict[color] = idx
        idx += 1
    end
    # display(sort(dict))

    # Build adjacency matrix
    nbcolors = length(dict)
    adjmatrix = zeros(Int, nbcolors, nbcolors)
    for (nbline, line) in enumerate(lines)
        spl = split(line)
        for (idx, word) in enumerate(spl)
            nb = tryparse(Int, word)
            if nb != nothing
                color = spl[idx+1]*spl[idx+2] # concatenate the 2 words composing the color
                adjmatrix[nbline, dict[color]] = nb
            end
        end
    end
    # display(adjmatrix)

    # useless binmatrix ?
    # binmatrix = [x > 0 ? 1 : 0 for x in adjmatrix]

    # Count "good" bags, that is bags that could contain shinygold, directly or indirectly
    countedbags = Set{Int64}()
    goodbags = Set{Int64}()
    push!(goodbags, dict["shinygold"])
    while !isempty(goodbags)
        curbag = pop!(goodbags)
        curcol = adjmatrix[:,curbag]
        for (idx, val) in enumerate(curcol)
            if val > 0
                push!(goodbags, idx)
                push!(countedbags, idx)
            end
        end
    end
    counter = length(countedbags)

    return counter
end

function puzzle2(datafilename)
    # Read data
    file = open(datafilename)
    lines = readlines(file)

    # Build a dictionary of colors
    dict = Dict{String,Int}()
    idx = 1
    for line in lines
        spl = split(line)
        color = spl[1]*spl[2]
        dict[color] = idx
        idx += 1
    end
    # display(sort(dict))

    # Build adjacency matrix
    nbcolors = length(dict)
    adjmatrix = zeros(Int, nbcolors, nbcolors)
    for (nbline, line) in enumerate(lines)
        spl = split(line)
        for (idx, word) in enumerate(spl)
            nb = tryparse(Int, word)
            if nb != nothing
                color = spl[idx+1]*spl[idx+2] # concatenate the 2 words composing the color
                adjmatrix[nbline, dict[color]] = nb
            end
        end
    end
    # display(adjmatrix)

    # useless binmatrix ?
    # binmatrix = [x > 0 ? 1 : 0 for x in adjmatrix]

    # Count bags inside shinygold (recursively)
    counter = 0
    bagstoexplore = Array{Int}(undef, 0)
    push!(bagstoexplore, dict["shinygold"])
    while !isempty(bagstoexplore)
        curbag = pop!(bagstoexplore)
        curcol = adjmatrix[curbag,:]
        for (idx, val) in enumerate(curcol)
            if val > 0
                # todo multiply val by right amount
                counter += val
                for _ in 1:val
                    push!(bagstoexplore, idx)
                end
            end
        end
    end

    return counter
end


println("Toy ex")
display(puzzle1("./toyexample")) # should output 4

datafilename = "./input"
println("\nPuzzle 1")
display(puzzle1(datafilename))

println("\nToy ex 2")
display(puzzle2("./toyexample2")) # should output 126

println("\nPuzzle 2")
display(puzzle2(datafilename))
