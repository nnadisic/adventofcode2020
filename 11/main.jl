using DelimitedFiles

function puzzle1(datafilename)
    # Read data
    data = readdlm(datafilename)
    nbrow = size(data,1)
    nbcol = length(data[1])
    # Build matrix of characters
    matchars = Matrix{Char}(undef, nbrow, nbcol)
    for i in 1:nbrow, j in 1:nbcol
        matchars[i,j] = data[i][j]
    end

    # Loop until seats organization is stable
    prevgrid = copy(matchars)
    curgrid = similar(prevgrid) # create matrix of same size and type
    stable = 0
    while stable == 0
        # Loop on all seats to update them
        for i in 1:nbrow, j in 1:nbcol
            neighbors = prevgrid[max(1,i-1):min(nbrow,i+1),max(1,j-1):min(nbcol,j+1)]
            if prevgrid[i,j] == 'L' && count(==('#'), neighbors) == 0
                curgrid[i,j] = '#'
            elseif prevgrid[i,j] == '#' && count(==('#'), neighbors) >= 5 # 5 instead of 4 to take into account the seat [i,j]
                curgrid[i,j] = 'L'
            else
                curgrid[i,j] = prevgrid[i,j]
            end
        end
        if prevgrid == curgrid
            stable = 1
        end
        prevgrid = copy(curgrid)
    end

    return count(==('#'), curgrid)
end


function puzzle2(datafilename)
    # Read data
    data = readdlm(datafilename)
    nbrow = size(data,1)
    nbcol = length(data[1])
    # Build matrix of characters
    matchars = Matrix{Char}(undef, nbrow, nbcol)
    for i in 1:nbrow, j in 1:nbcol
        matchars[i,j] = data[i][j]
    end

    # Loop until seats organization is stable
    prevgrid = copy(matchars)
    curgrid = similar(prevgrid) # create matrix of same size and type
    stable = 0
    while stable == 0
        # Loop on all seats to update them
        for i in 1:nbrow, j in 1:nbcol
            neighbors = generatevisibleneighbors(prevgrid, i, j)
            if prevgrid[i,j] == 'L' && count(==('#'), neighbors) == 0
                curgrid[i,j] = '#'
            elseif prevgrid[i,j] == '#' && count(==('#'), neighbors) >= 5
                curgrid[i,j] = 'L'
            else
                curgrid[i,j] = prevgrid[i,j]
            end
        end
        if prevgrid == curgrid
            stable = 1
        end
        prevgrid = copy(curgrid)
    end

    return count(==('#'), curgrid)
end

function generatevisibleneighbors(prevgrid, i, j)
    nbrow, nbcol = size(prevgrid)
    visibleseats = []
    # Go up
    for k in i-1:-1:1
        if prevgrid[k,j] != '.'
            push!(visibleseats, prevgrid[k,j])
            break
        end
    end
    # Go down
    for k in i+1:nbrow
        if prevgrid[k,j] != '.'
            push!(visibleseats, prevgrid[k,j])
            break
        end
    end
    # Go left
    for l in j-1:-1:1
        if prevgrid[i,l] != '.'
            push!(visibleseats, prevgrid[i,l])
            break
        end
    end
    # Go right
    for l in j+1:nbcol
        if prevgrid[i,l] != '.'
            push!(visibleseats, prevgrid[i,l])
            break
        end
    end
    # Go diag up-right
    for d in 1:min(i-1,nbcol-j)
        k = i - d
        l = j + d
        if prevgrid[k,l] != '.'
            push!(visibleseats, prevgrid[k,l])
            break
        end
    end
    # Go diag down-right
    for d in 1:min(nbrow-i,nbcol-j)
        k = i + d
        l = j + d
        if prevgrid[k,l] != '.'
            push!(visibleseats, prevgrid[k,l])
            break
        end
    end
    # Go diag down-left
    for d in 1:min(nbrow-i,j-1)
        k = i + d
        l = j - d
        if prevgrid[k,l] != '.'
            push!(visibleseats, prevgrid[k,l])
            break
        end
    end
    # Go diag up-left
    for d in 1:min(i-1,j-1)
        k = i - d
        l = j - d
        if prevgrid[k,l] != '.'
            push!(visibleseats, prevgrid[k,l])
            break
        end
    end

    return visibleseats
end



display(puzzle1("./toyex")) # should give 37
display(puzzle1("./input"))

display(puzzle2("./toyex")) # should give 26
display(puzzle2("./input"))
