function puzzle(filename; part=1)
    # Read data
    file = open(filename)
    rawdata = read(file, String)
    rawrules, rawmyticket, rawnearby = split(rawdata, "\n\n")

    # Store rules as entries of size-4 vectors
    rules = Vector{Vector{Int}}(undef, 0)
    for r in split(rawrules, "\n")
        rsplit = split(split(r, ":")[2])
        l1, u1 = split(rsplit[1], "-")
        l2, u2 = split(rsplit[3], "-")
        push!(rules, [parse(Int, l1), parse(Int, u1), parse(Int, l2), parse(Int, u2)])
    end
    # @show rules

    # Read my ticket
    myticket = [parse(Int, x) for x in split(split(rawmyticket, "\n")[2], ",")]
    # @show myticket

    # Read nearby tickets
    nearbytickets = Vector{Vector{Int}}(undef, 0)
    nearbystr = split(rawnearby, "\n")
    for i in 2:length(nearbystr)
        push!(nearbytickets, [parse(Int, x) for x in split(nearbystr[i], ",")])
    end
    # @show nearbytickets

    # Loop on tickets to find invalid numbers
    validtickets = Vector{Vector{Int}}(undef, 0) # store valid tickets
    suminvalid = 0
    for t in nearbytickets
        isticketvalid = true
        for n in t
            isnumbervalid = false
            for r in rules
                if (r[1] <= n <= r[2]) || (r[3] <= n <= r[4])
                    isnumbervalid = true
                    break
                end
            end
            if !isnumbervalid
                isticketvalid = false
                suminvalid += n
            end
        end
        if isticketvalid
            push!(validtickets, t)
        end
    end

    # For part 1 of puzzle, stop here and return sum of invalid numbers
    if part == 1
        return suminvalid
    end

    ######################################################################
    # For part 2, identify which field correspond with which position

    # Store nearbytickets in a matrix, so that every row is a field
    matnearbytickets = reshape(collect(Iterators.flatten(validtickets)),
                               :, length(validtickets))
    # Store the correspondance field-position (key = line number of field in input, value = vector of valid positions)
    fieldpositions = Dict{Int, Vector{Int}}()

    # Loop on rules to find correspondances
    for (ir, r) in enumerate(rules)
        fieldpositions[ir] = Int[]
        for j in 1:size(matnearbytickets, 1)
            row = matnearbytickets[j,:]
            isrowvalid = true
            for n in row
                if !((r[1] <= n <= r[2]) || (r[3] <= n <= r[4]))
                    isrowvalid = false
                    break
                end
            end
            if isrowvalid
                push!(fieldpositions[ir], j)
            end
        end
        sort!(fieldpositions[ir])
    end

    # Find for each field a unique position (position not valid in other fields)
    uniquefp = Dict{Int, Int}()
    # Sort fields by number of possible positions
    sortedfp = sort(collect(fieldpositions), by=x->length(x[2]))
    # display(sortedfp)
    alreadyselected = Vector{Int}(undef, 0)
    for fp in sortedfp
        for p in fp[2]
            if !in(p, alreadyselected)
                uniquefp[fp[1]] = p
                push!(alreadyselected, p)
            end
        end
    end

    # Compute the product of the values in my ticket corresponding to first 6 fields
    prod = 1
    for i in 1:min(6,length(fieldpositions)) # min: safety if less than 6 fields (eg in toy)
        prod *= myticket[uniquefp[i]]
    end

    # Return
    return prod
end

println("Puzzle 1")
display(puzzle("./toy")) # should give 71
display(puzzle("./input"))

println("\nPuzzle 2")
display(puzzle("./toy2", part=2)) # should give 12*11*13 = 1716
display(puzzle("./input", part=2))
