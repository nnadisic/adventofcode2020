using DelimitedFiles

function puzzle1(datafilename)
    # Read data
    data = readdlm(datafilename, BigInt)
    sizedata = length(data)

    # Find the first number which is NOT the sum of two of the 25 numbers before it
    for idx in 26:sizedata
        valid = 0
        # Test all possible combinations for sum
        for i in idx-25:idx-1, j in i:idx-1
            cursum = data[i] + data[j]
            if data[idx] == cursum && data[i] != data[j]
                valid = 1
            end
        end
        if valid == 0
            return data[idx]
        end
    end

    return 0
end

function puzzle2(datafilename)
    # Read data
    data = readdlm(datafilename, BigInt)
    sizedata = length(data)

    # Answer from puzzle1
    invalidnumber = 258585477

    # Find the set of contiguous numbers that sum to invalidnumber
    for i in 1:sizedata
        for j in i+1:sizedata
            curset = data[i:j]
            cursum = sum(curset)
            if cursum > invalidnumber
                break
            elseif cursum == invalidnumber
                return minimum(curset) + maximum(curset)
            end
        end
    end

    return 0
end



datafilename = "./input"
display(puzzle1(datafilename))
display(puzzle2(datafilename))
